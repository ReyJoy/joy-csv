﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GoogleSheetFetcher.Editor;
using Joyseed.Csv;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class CsvScriptableObject<T> : ScriptableObject
{
    public string spreadsheetId;
    [ReadOnly]
    public T[] data;
    
    private static Fetcher _fetcher;
    public bool DidFetcherInitialize => _fetcher?.DidInitialize ?? false;
    
#if UNITY_EDITOR
    [Button(ButtonSizes.Large)]
    public void ReloadData()
    {
        if (DidFetcherInitialize)
        {
            LogAllValuesOfFirstSheetAsync();
        }
        else
        {
            InitializeAsync(delegate
            {
                LogAllValuesOfFirstSheetAsync();
            });
        }
    }
    
    [Button(ButtonSizes.Large)]
    public void ClearData()
    {
        data = null;
    }

    private async Task InitializeAsync(Action callback)
    {
        try
        {
            _fetcher = new Fetcher();
            await _fetcher.InitializeAsync(JoyCsvConfig.Instance.clientId, JoyCsvConfig.Instance.clientSecret, JoyCsvConfig.Instance.appId).Task;
            callback?.Invoke();
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private async Task LogAllValuesOfFirstSheetAsync()
    {
        try
        {
            var sheets = await _fetcher.FetchSheetsAsync(spreadsheetId).Task;
            var values = await _fetcher.FetchValuesAsync(spreadsheetId, sheets[0]).Task;
            var csvString = "";
            for (var i = 0; i < values.Count; i++)
            {
                var row = values[i];
                csvString += row.Count >= 1 ? row.Aggregate((a, b) => $"{a},{b}") : "";
                if(i != values.Count - 1)
                    csvString += "\n";
            }
            Debug.Log(csvString);
            data = CSVSerializer.Deserialize<T>(csvString);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }
#endif

    /*private void ReloadFromFile()
    {
        var allLines = File.ReadAllText(Application.dataPath + "/" + csvFile);
        data = CSVSerializer.Deserialize<T>(allLines);
        Debug.Log($"Reload done...");
    }

    private void ReloadFromWeb()
    {
#if UNITY_EDITOR
        EditorCoroutines.Execute(DownloadAndImport());
#endif
    }

    private IEnumerator DownloadAndImport()
    {
        UnityWebRequest www = UnityWebRequest.Get(csvUrl);
        yield return www.SendWebRequest();

        while (www.isDone == false)
        {
            yield return new WaitForEndOfFrame();
        }

        if (www.error != null)
        {
            Debug.Log("UnityWebRequest.error:" + www.error);
        }
        else if (www.downloadHandler.text == "" || www.downloadHandler.text.IndexOf("<!DOCTYPE") != -1)
        {
            Debug.Log("Uknown Format:" + www.downloadHandler.text);
        }
        else
        {
            var rows = CSVSerializer.ParseCSV(www.downloadHandler.text);
            data = CSVSerializer.Deserialize<T>(rows);
            Debug.Log($"Reload done...");
        }
    }*/
}
