﻿using Sirenix.Utilities;

namespace Joyseed.Csv {
    [GlobalConfig("Resources/")]
    public class JoyCsvConfig : GlobalConfig<JoyCsvConfig>
    {
        public string clientId;
        public string clientSecret;
        public string appId;
    }
}